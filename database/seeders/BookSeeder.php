<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authorIds = Author::pluck('id')->toArray();
        $categoryIds = Category::pluck('id')->toArray();

        foreach ($authorIds as $authorId) {
            foreach ($categoryIds as $categoryId) {
                Book::factory(2)->create([
                    'author_id' => $authorId,
                    'category_id' => $categoryId,
                ]);
            }
        }
    }
}
