<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        // $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        // $this->call(SettingSeeder::class);
        // $this->call(SeoMetaSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(AuthorSeeder::class);
        $this->call(BookSeeder::class);
        $this->call(CmsAdminSeeder::class);
    }
}
