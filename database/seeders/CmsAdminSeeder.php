<?php

namespace Database\Seeders;

use App\Models\CmsAdmin;
use Illuminate\Database\Seeder;

class CmsAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creating user admin and granting super-administrator role
        CmsAdmin::factory()->create()->assignRole('super-administrator');
    }
}
