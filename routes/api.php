<?php

use Illuminate\Support\Facades\Route;

use function Clue\StreamFilter\fun;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function() {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('forgot', 'PasswordController@forgot');
    Route::post('reset', 'PasswordController@reset');
    Route::post('logout', 'AuthController@logout')->middleware('auth:sanctum');
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::apiResource('/categories', 'CategoriesController');
    Route::apiResource('/books', 'BooksController');
    Route::apiResource('/authors', 'AuthorsController');
});

