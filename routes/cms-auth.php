<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| CMS Auth Routes
|--------------------------------------------------------------------------
|
| Here is where you can register cms auth routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login', 'CmsAuth\LoginController@login')->name('login');
Route::post('logout', 'AuthController@logout')->name('logout')->middleware('auth:sanctum');
// Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::post('password/reset', 'ResetPasswordController@reset')->name('password.update');
