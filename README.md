![Build](https://github.com/richan-fongdasen/laravel-api-starter/workflows/CI/badge.svg?branch=master) 
[![codecov](https://codecov.io/gh/richan-fongdasen/laravel-api-starter/branch/master/graph/badge.svg?token=geukYN4Ubz)](https://codecov.io/gh/richan-fongdasen/laravel-api-starter) 
[![StyleCI](https://github.styleci.io/repos/272756462/shield?branch=master)](https://github.styleci.io/repos/272756462) 
[![License: MIT](https://poser.pugx.org/laravel/framework/license.svg)](https://opensource.org/licenses/MIT) 

# Laravel API Starter

