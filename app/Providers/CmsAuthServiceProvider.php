<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\CmsAdmin;
use App\Models\SeoMeta;
use App\Models\Setting;
use App\Models\Book;
use App\Models\Author;
use App\Policies\Cms\CategoryPolicy;
use App\Policies\Cms\CmsAdminPolicy;
use App\Policies\Cms\RolePolicy;
use App\Policies\Cms\SeoMetaPolicy;
use App\Policies\Cms\SettingPolicy;
use App\Policies\Cms\BookPolicy;
use App\Policies\Cms\AuthorPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Spatie\Permission\Models\Role;

class CmsAuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        CmsAdmin::class => CmsAdminPolicy::class,
        Role::class     => RolePolicy::class,
        SeoMeta::class  => SeoMetaPolicy::class,
        Setting::class  => SettingPolicy::class,
        Category::class => CategoryPolicy::class,
        Author::class => AuthorPolicy::class,
        Book::class => BookPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
