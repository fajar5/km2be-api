<?php

namespace App\Policies\Cms;

use App\Models\CmsAdmin;
use App\Models\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy extends AbstractPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the CMS Admin can view any models.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     *
     * @return bool
     */
    public function viewAny(CmsAdmin $cmsAdmin): bool
    {
        return $this->can($cmsAdmin, new Category(), 'viewAny');
    }

    /**
     * Determine whether the CMS Admin can view the model.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     * @param \App\Models\Category $category
     *
     * @return bool
     */
    public function view(CmsAdmin $cmsAdmin, Category $category): bool
    {
        return $this->can($cmsAdmin, $category, 'view');
    }

    /**
     * Determine whether the CMS Admin can create models.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     *
     * @return bool
     */
    public function create(CmsAdmin $cmsAdmin): bool
    {
        return $this->can($cmsAdmin, new Category(), 'create');
    }

    /**
     * Determine whether the CMS Admin can update the model.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     * @param \App\Models\Category $category
     *
     * @return bool
     */
    public function update(CmsAdmin $cmsAdmin, Category $category): bool
    {
        return $this->can($cmsAdmin, $category, 'update');
    }

    /**
     * Determine whether the CMS Admin can delete the model.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     * @param \App\Models\Category $category
     *
     * @return bool
     */
    public function delete(CmsAdmin $cmsAdmin, Category $category): bool
    {
        return $this->can($cmsAdmin, $category, 'delete');
    }

    /**
     * Determine whether the CMS Admin can restore the model.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     * @param \App\Models\Category $category
     *
     * @return bool
     */
    public function restore(CmsAdmin $cmsAdmin, Category $category): bool
    {
        return $this->can($cmsAdmin, $category, 'restore');
    }

    /**
     * Determine whether the CMS Admin can permanently delete the model.
     *
     * @param \App\Models\CmsAdmin $cmsAdmin
     * @param \App\Models\Category $category
     *
     * @return bool
     */
    public function forceDelete(CmsAdmin $cmsAdmin, Category $category): bool
    {
        return $this->can($cmsAdmin, $category, 'forceDelete');
    }
}
