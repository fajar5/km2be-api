<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookGetRequest extends FormRequest
{
    /**
     * Determine if the current user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
//        return (auth()->guard('api')->check() || auth()->guard('cms-api')->check());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter.id' => 'integer|between:0,4294967295',
            'filter.author_id' => 'integer|between:-2147483647,2147483647',
            'filter.category_id' => 'integer|between:-2147483647,2147483647',
            'filter.title' => 'string|min:2|max:255',
            'filter.release_date' => 'date',
            'filter.created_at' => 'date',
            'filter.updated_at' => 'date',
            'filter.books\.id' => 'integer|between:0,4294967295',
            'filter.books\.author_id' => 'integer|between:-2147483647,2147483647',
            'filter.books\.category_id' => 'integer|between:-2147483647,2147483647',
            'filter.books\.title' => 'string|min:2|max:255',
            'filter.books\.release_date' => 'date',
            'filter.books\.created_at' => 'date',
            'filter.books\.updated_at' => 'date',
            'filter.author\.id' => 'integer|between:-2147483647,2147483647',
            'filter.author\.name' => 'string|min:2|max:255',
            'filter.author\.created_at' => 'date',
            'filter.author\.updated_at' => 'date',
            'filter.category\.id' => 'integer|between:-2147483647,2147483647',
            'filter.category\.name' => 'string|min:2|max:255',
            'filter.category\.slug' => 'string|regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|min:2|max:255',
            'filter.category\.created_at' => 'date',
            'filter.category\.updated_at' => 'date',
            'page.number' => 'integer|min:1',
            'page.size' => 'integer|between:1,100',
            'search' => 'nullable|string|min:3|max:60',
        ];
    }
}
