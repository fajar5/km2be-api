<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookSaveRequest extends FormRequest
{
    /**
     * Determine if the current user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
        //        return (auth()->guard('api')->check() || auth()->guard('cms-api')->check());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'author_id' => 'required|integer|between:1,2147483647|exists:authors,id',
            'category_id' => 'required|integer|between:1,2147483647|exists:categories,id',
            'title' => 'required|string|min:2|max:255',
            'release_date' => 'required|string|date_format:Y-m-d',
        ];
    }
}
