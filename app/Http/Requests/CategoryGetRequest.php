<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryGetRequest extends FormRequest
{
    /**
     * Determine if the current user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
//        return (auth()->guard('api')->check() || auth()->guard('cms-api')->check());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter.id' => 'integer|between:1,4294967295',
            'filter.name' => 'string|min:2|max:255',
            'filter.slug' => 'string|regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|min:2|max:255',
            'filter.created_at' => 'date',
            'filter.updated_at' => 'date',
            'filter.categories\.id' => 'integer|between:1,4294967295',
            'filter.categories\.name' => 'string|min:2|max:255',
            'filter.categories\.slug' => 'string|regex:/^[a-z0-9]+(?:-[a-z0-9]+)*$/|min:2|max:255',
            'filter.categories\.created_at' => 'date',
            'filter.categories\.updated_at' => 'date',
            'page.number' => 'integer|min:1',
            'page.size' => 'integer|between:1,100',
            'search' => 'nullable|string|min:3|max:60',
        ];
    }
}
