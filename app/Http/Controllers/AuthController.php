<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Notifications\NewUserNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(UserRegisterRequest $request, User $user): JsonResponse {
        $user->fill($request->only($user->offsetGet('fillable')))->save();
        $user->notify(new NewUserNotification($user));
        $resource = (new UserResource($user))->additional(['info' => 'The user has been created.']);

        return $resource->toResponse($request)->setStatusCode(201);
    }

    public function login(UserLoginRequest $request): JsonResponse {
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json(['message' => 'Credentials failed'], 401);
        }

        return (new UserResource($user))->additional([
            'info' => 'The user has been logged in.',
            'token' => $user->createToken('ebook')->plainTextToken,
            'token_type' => 'Bearer',
        ])->toResponse($request)->setStatusCode(200);
    }

    public function logout(): JsonResponse {
        Auth::user()?->tokens()->delete();
        return response()->json([
            'message' => 'You have successfully logged out and the token was successfully deleted.'
        ], 200);
    }
}
