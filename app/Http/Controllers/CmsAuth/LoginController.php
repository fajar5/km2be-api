<?php

namespace App\Http\Controllers\CmsAuth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Http\Resources\CmsAdminResource;
use App\Models\CmsAdmin;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Authenticate user when doing login
     */
    public function login(UserLoginRequest $request): JsonResponse
    {
        $admin = CmsAdmin::where('email', $request->email)->first();

        if (!$admin || !Hash::check($request->password, $admin->password) || !$admin->hasRole('super-administrator')) {
            return response()->json(['message' => 'Credentials failed'], 401);
        }

        return (new CmsAdminResource($admin))->additional([
            'info' => 'The user has been logged in.',
            'token' => $admin->createToken(config('api.cms_guard'))->plainTextToken,
            'token_type' => 'Bearer',
        ])->toResponse($request)->setStatusCode(200);
    }


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard|\Illuminate\Contracts\Auth\Guard
     */
    protected function guard()
    {
        return auth()->guard(config('api.cms_guard'));
    }

    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $rules = [
            $this->username() => 'required|string|email|min:11',
            'password'        => 'required|string|min:6',
        ];

        if (config('api.captcha_enabled')) {
            $rules['g-recaptcha-response'] = 'required|captcha';
        }

        $request->validate($rules);
    }
}
