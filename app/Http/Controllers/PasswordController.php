<?php

namespace App\Http\Controllers;

use App\Events\ForgotPasswordEvent;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class PasswordController extends Controller
{
    public function forgot(ForgotPasswordRequest $request): JsonResponse {
        event(new ForgotPasswordEvent($request->email));
        return response()->json([
            'message' => 'We have sent you an email with a link to reset your password.'
        ], 200);
    }

    public function reset(ResetPasswordRequest $request): JsonResponse {
        $data = DB::table('password_resets')
            ->where('email', $request->email)
            ->where('token', $request->token)
            ->first();
        
        if (!$data) {
            return response()->json(['message' => 'Invalid token or email'], 422);
        }

        User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);
        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return response()->json(['message' => 'The user has been updated.'], 200);
    }
}
