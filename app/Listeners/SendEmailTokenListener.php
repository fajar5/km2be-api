<?php

namespace App\Listeners;

use App\Models\User;
use App\Notifications\ForgotPasswordNotification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class SendEmailTokenListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $resetToken = Str::random(60);
        $user = User::where('email', $event->email)->get();

        DB::table('password_resets')->insert([
            'email' => $event->email,
            'token' => $resetToken,
            'created_at' => Carbon::now()
        ]);

        Notification::send($user, new ForgotPasswordNotification($resetToken));
    }
}
